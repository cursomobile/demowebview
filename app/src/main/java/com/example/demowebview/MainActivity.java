package com.example.demowebview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView mywebview = (WebView) findViewById(R.id.webView);

        //Ejemplo crear una web
        //String data = "<html><body><h1>Hello,Javatpoint!!</h1></body></html>";
        //mywebview.loadData(data, "text/html", "UTF-8");

        //Ejemplo abrir desde html
        mywebview.loadUrl("file:///android_asset/myfile.html");

        //Ejemplo abrir una pagina existente
        //mywebview.loadUrl("https://www.javatpoint.com/");
    }
}
